# ISOLATION API
## _Secure your Browser_
## Installation

Requires [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/install/) to run.

##### Set params on .env file
```sh
BACKEND_IP=SERVER-IP-HERE
```

```sh
BACKEND_PORT=8080
```

```sh
BACKEND_CONTEXT=back
```

For a quick start use.

```sh
sudo sh ./configure.sh
```
```sh
./start.sh
```

To apply changes in isolated sytes use

```sh
sh ./apply-dns.sh
```

## Plugins

Isolation api is currently using following technologies.

| Plugin | README |
| ------ | ------ |
| Isolation-api | https://gitlab.com/erickaguanoluisa92/isolation-api |
| Web-isolation-manager-front-end | https://gitlab.com/tesis8/web-isolation-manager-front-end |
| Web-isolation-manager | https://gitlab.com/tesis8/web-isolation-manager |
| Web-isolation-redirecter | https://gitlab.com/tesis8/web-isolation-redirecter |
| Firebase | https://firebase.google.com/ |
| Dnsmasq | https://github.com/jpillora/docker-dnsmasq |
| Nginx | https://www.nginx.com/ |

## Development

Want to contribute? Great!

Isolation api uses Flutter (gradle) + Springboot (gradle) for fast developing.

Clone this 3 proyects.

Isolation-api:

```sh
git clone https://gitlab.com/erickaguanoluisa92/isolation-api
./start.sh
```

Web-isolation-manager-front-end:

```sh
git clone https://gitlab.com/tesis8/web-isolation-manager-front-end
flutter run
```

Web-isolation-manager:

```sh
git clone https://gitlab.com/tesis8/web-isolation-manager
./gradlew bootRun
```

#### Building docker image

Web-isolation-manager-front-end:

```sh
flutter build web && docker build -t registry.gitlab.com/tesis8/web-isolation-manager-front-end .
```

Web-isolation-manager:

```sh
./gradlew bootjar &&  docker build -t registry.gitlab.com/tesis8/web-isolation-manager .
```

## License

   This software is released under version 3 of the GNU Affero General
   Public License (AGPLv3), as set forth below, with the following
   additional permissions:

   This distribution of Isolation api is distributed with certain software
   (including but not limited to Node.JS) that is licensed under separate
   terms, as designated in a particular file or component or in the
   license documentation. Without limiting your rights under the AGPLv3,
   the authors of Isolation api hereby grant you an additional permission to link
   the program and your derivative works with the separately licensed
   software that they have included with the program.

   This distribution includes the Isolation api client application
   (voodoo) otherwise known as Isolation api Voodoo. Without limiting
   the foregoing grant of rights under the AGPLv3 and additional permission
   as to separately licensed software, this Voodoo is also subject to
   the Universal FOSS Exception, version 1.0, a copy of which is
   reproduced below and can also be found along with its FAQ at
   http://oss.oracle.com/licenses/universal-foss-exception.

   Copyright (c) 2021, Ericka Liseth Guanoluisa.

Election of AGPLv3

   For the avoidance of doubt, except that if any license choice other
   than AGPL is available it will apply instead, We elects to
   use only the Affero General Public License version 3 (AGPLv3) at this time 
   for inherit it from a ViewFinder (https://github.com/i5ik/ViewFinderJS/) fork
   in Comunity Edition (https://github.com/EmilMarian/browsergap.ce)
   any software where a choice of AGPL license versions is made available
   with the language indicating that AGPLv3 or any later version may be
   used, or where a choice of which version of the AGPL is applied is
   otherwise unspecified. 
