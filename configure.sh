sudo su -c "echo 'kernel.unprivileged_userns_clone=1' > /etc/sysctl.d/00-local-userns.conf"
sudo su -c "echo 'net.ipv4.ip_forward=1' > /etc/sysctl.d/01-network-ipv4.conf"
sudo sysctl -p
sleep 1
echo '[Resolve]' > /etc/systemd/resolved.conf
echo 'DNS=IP_ADDR_HERE' >> /etc/systemd/resolved.conf
echo 'DNS=1.1.1.1' | sudo tee -a /etc/systemd/resolved.conf
echo 'DNSStubListener=no' | sudo tee -a /etc/systemd/resolved.conf
sed -i 's/IP_ADDR_HERE/'$(head -1 .env | cut -b 12-30)'/g' /etc/systemd/resolved.conf
sudo systemctl restart systemd-resolved.service
#sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
sudo systemctl restart systemd-networkd.service
sudo systemctl stop systemd-resolved
sudo systemctl disable systemd-resolved
sed -i 's/IP_ADDR_HERE/'$(head -1 .env | cut -b 12-30)'/g' ./nginx/conf.d/redirect.conf
